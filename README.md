## Practica Prueba BDA

# Idea General

Buscador de pisos de airbnb para acudir a las actividades culturales y de ocio propuestas por el ayuntamiento de Madrid que tengan cerca aparcamientos publicos.

Basándose en las actividades culturales y de ocio del ayuntamiento de Madrid, sacar las actividades del próximo mes, recomendar apartamentos de airbnb cerca
y, si es posible, mostrar aparcamientos públicos cercanos.

# Nombre del Producto

ActCulMad = Actividades Culturales Madrid.
Recomendador de Airbnb para ActCulMad. 

### Estrategia del DAaas

A primeros de cada mes sacar un informe con las actividades culturales del próximo mes con los pisos de airbnb y aparcamientos públicos mejor situados
para acudir a estas actividades.

Utilizar herramientas en la nube para facilitar el manejo de los datos.

### Arquitectura

Arquitectura Cloud basada en API + Google Cloud Storage + HIVE + Dataproc.

Usar APIs para leer datos publicos de actividades culturales y aparcamientos publicos del ayuntamiento de Madrid obteniendo los resultados en csv.
Ejecutarlo en un Cloud Function.

Insertar el dataset de airbnb en HIVE.

Colocar el resultado de los APIs y del dataset de airbnb en un segmento de Google Cloud.

Desde Google Storage coger los datos para crear 3 tablas de HIVE y realizar, no se cómo todavía, una query con un JOIN que reste las distancias entre cada actividad cultural
y los pisos de airbnb y los aparcamientos. (Hacer todo en 1 JOIN, 2 JOIN y fusionarlos???)

De la query final obtener las actividades culturales del próximo mes con los 5 apartamentos de airbnb y los 3 aparcamientos más cercanos.

El resultado de la query estara en Google Storage.

### Operating Model

El operador va a disparar el cloud function de actividades una vez al mes, a primeros de mes, para obtener las acividades culturales y de ocio del mes siguiente.
Esto disparara el cloud function y guardara el resultado en un directorio del segmento llamado "input_actividades".

El operador va a disparar el cloud function de aparcamientos una vez al año para obtener los aparcamientos municipales del año siguiente.
Esto disparara el cloud function y guardara el resultado en un directorio del segmento llamado "input_aparcamientos".

En el segmento siempre habra un directorio llamado "input_airbnb".

Seguir el standard de levantar el Cluster solamente cuando quiera regenerar el listado de actividades mensual.

Una vez al mes, levantar el CLUSTER a mano, enviar las tareas de:

- Crear tablas de airbnb, actividades y aparcamientos.
- load data inpath de gs://XXXX:input_actividades/ into table actividades
- load data inpath de gs://XXXX:input_airbnb/ into table airbnb
- load data inpath de gs://XXXX:input_aparcamientos/ into table aparcamientos
- SELECT JOIN INTO DIRECTORY 'gs://output/results'

Apagar el cluster.

Montar una web con un link directo al Google Storage Segment Object, o algo similar, para visualizar los datos.

### Desarrollo

Tengo el cloud function aqui: ???
Query de HIVE ???
Pantallazo final ???

### Diagrama

https://drive.google.com/file/d/19nQ2epeIGmwEDW9VP4PXI3o9bk4E1WhE/view?usp=sharing

